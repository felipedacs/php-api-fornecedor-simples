<?php
//http://clubedosgeeks.com.br/programacao/php/api-restful-com-php-e-slim-framework
//https://tableless.com.br/php-slim-framework/

require_once('vendor/autoload.php');


$app = new \Slim\Slim(array(
'templates.path' => 'templates'
));

$app->get('/', function () use ($app){
  //defina
  $data = array("data"=>array("Hello"=>"World!"));
  //set o arquivo de template
  $app->render('default.php',$data,200);
});

$app->get('/calcular/:valorInicial', function ($valorInicial) use ($app){
  //defina
  $resultado = $valorInicial * 2;
  $jsonResponse = array(
    'valorInicial' => $valorInicial,
    'resultado' => $resultado,
  );
  $data = array('data' => $jsonResponse );

  $app->render('default.php',$data,200);
});

$app->group('/visualizar',function() use ($app){
  //rota para a home
  $app->get('/',function() use ($app){
    //exemplo de lista de usuarios
    $users = array(
     'users'=>array(
       'jo'=>'senhadejo',
       'luca'=>'senhaluca',
       'yasmin'=>'senhayasmin',
       'eric'=>'seric'
     )
    );
    $data = array(
      'data'=>$users
      );
    $app->render('default.php',$data,200);
  });

  //rota para login
  $app->post('/login/',function() use ($app){
    if(isset($_POST))
    {
      $data = $_POST;
      $app->render('default.php',$data,200);
    }
    else
    {
      $app->render(404);
    }
  });

});


/*
$app = new \Slim\Slim();
$app->group('/users',function() use ($app){

  //rota para a home
  $app->get('/teste/:msg',function($msg){
    //exemplo de lista de usuarios
    /*
    $users = array(
     'users'=>array(
       'jo'=>'senhadejo',
       'luca'=>'senhaluca',
       'yasmin'=>'senhayasmin',
       'eric'=>'seric'
     )
    );
    $data = array(
      'data'=>$users
      );
    $app->render('default.php',$data,200);

    echo $msg;
  });

  //rota para login
  /*
  $app->post('/login/',function() use ($app){

    if(isset($_POST))
    {
      $data = $_POST;
      $app->render('default.php',$data,200);
    }
    else
    {
      $app->render(404);
    }

  });


});
*/
$app->run();
 ?>
